#!/usr/bin/bash

border()
{
    title="| $1 |"
    # shellcheck disable=SC2001
    edge='+'`head -c $(( ${#title}-2 )) </dev/zero|tr '\0' '-'`'+'
    echo "$edge"
    echo "$title"
    echo "$edge"
}

border $1
