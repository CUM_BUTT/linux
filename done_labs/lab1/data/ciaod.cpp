#include <iostream>

using namespace std;

auto DigitToBinString(unsigned digit)
{
    string result = "";
    for (; digit; digit >>= 1)
    {       
        string buff = ((digit % 2) ? "1" : "0");
        result = buff + result;
    }
    return result;
}

unsigned BinStringToDigit(string s)
{
    unsigned a = 1, n = 0;
   for(int i=s.size()-1;i>=0;i--)
   {
    if(s[i]!=48 && s[i]!=49)
    return 0;
    n+=(s[i]-'0')*a;
     a*=2;
   }
    return n;
}

auto Or(unsigned digit, unsigned mask)// беззнаковое целое число
{
    auto result =  digit | mask;
    return result;
}
auto Task2(unsigned digit, unsigned mask)// беззнаковое целое число
{
    mask = ~mask;
    auto result = digit & mask;
    return result;
}

unsigned Add(unsigned num1, unsigned num2) {
    unsigned res = 0, carry = 0;
    res = num1^num2;
    carry = (num1&num2) << 1;
    while (carry) {
        int tmp = res;
        res = res^carry;
        carry = (tmp&carry) << 1;
    }
    return res;
}

unsigned Multiply(unsigned num1, unsigned num2)
{   
    unsigned result = 0;
    for (size_t i = 0; i < num2; i++)
    {
        result = Add(result, num1);
    }
    return result;
}


// Находим противоположное число n
// ~: побитовое отрицание
// добавить: добавить операцию, добавить один к последнему биту
int Negtive(int n) {
    return Add(~n, 1);
}

int Subtraction(int a, int b) {
    // Добавить противоположный номер вычитаемого числа
    return Add(a, Negtive(b));
}

unsigned Division(unsigned digit, unsigned delimiter)
{
    unsigned result;
    for (result = 0; delimiter <= digit; digit -= delimiter, result++)
    {
        /* code */
    }
    return result;
}

unsigned SetOne(unsigned digit, unsigned posicion)
{
    unsigned mask = 1;
    for (size_t i = 0; i < posicion; i++)
    {
        mask <<= 1;
    }
    return Or(digit, mask);
}

string DigitToString(unsigned digit)
{
    string result = DigitToBinString(digit) + "(" + to_string(digit) + ")";
    return result;
}

void Task1()
{
    unsigned digit = 6;
    cout << "Input digit\n";
    cin >> digit;
    unsigned mask = BinStringToDigit("0101010101010101");
    unsigned result = Or(digit, mask);

    cout << "------------\nTask 1\n" 
        << "digit = " << DigitToString(digit) << endl 
        << "mask = " << DigitToString(mask) << endl 
        << "result = " << DigitToString(result) << endl;
}


void Task2()
{
    unsigned digit = 6;
    cout << "Input digit\n";
    cin >> digit;
    unsigned mask = BinStringToDigit("0000010101000000"); 
    unsigned result = Task2(digit, mask);

    cout << "------------\nTask 2\n" 
        << "digit = " << DigitToString(digit) << endl 
        << "mask = " << DigitToString(mask) << endl 
        << "result = " << DigitToString(result) << endl;

}

void Task3()
{
    unsigned digit = 6;
    cout << "Input digit\n";
    cin >> digit;
    unsigned mask = 16; 
    unsigned result = Multiply(digit, mask);

    cout << "------------\nTask 3\n" 
        << "digit = " << DigitToString(digit) << endl 
        << "multiplicator = " << DigitToString(mask) << endl 
        << "result = " << DigitToString(result) << endl;

}

void Task4()
{
    unsigned digit = 6;
    cout << "Input digit\n";
    cin >> digit;
    unsigned mask = 16; 
    unsigned result = Division(digit, mask);

    cout << "------------\nTask 4\n" 
        << "digit = " << DigitToString(digit) << endl 
        << "delimiter = " << DigitToString(mask) << endl 
        << "result = " << DigitToString(result) << endl;

}

void Task5()
{
    unsigned digit = 21845;
    unsigned mask = 1; // 0101 0101 0101 0101
    cout << "Input digit and posicion\n";
    cin >> digit >> mask;
    unsigned result = SetOne(digit, mask);

    cout << "------------\nTask 5\n" 
        << "digit = " << DigitToString(digit) << endl 
        << "posicion = " << DigitToString(mask) << endl 
        << "result = " << DigitToString(result) << endl;

}

int main()
{
    Task1();
    Task2();
    Task3();
    Task4();
    Task5();
}


