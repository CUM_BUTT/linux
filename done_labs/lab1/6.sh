#!/usr/bin/bash

file=/home/idegtyarev/Daniar/Linux/kisscm/done_labs/lab1/data/js.js
file=/home/idegtyarev/Daniar/Linux/kisscm/done_labs/lab1/data/python.py

expansion=$(echo -n $file | tail -c 3)
echo $expansion

file_content=$(cat $file)
echo $file_content

if [[ $expansion == ".py" && $(echo -n $file_content | head -c 1) == "#" ]]; then
  echo "py start from #";
  fi;
if [[ $expansion == ".js" && $(echo -n $file_content | head -c 2) == "//" ]]; then
  echo "js start from //";
  fi;